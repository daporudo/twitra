require 'sinatra'
require 'sinatra/reloader'
require 'mongoid'
require 'digest/md5'
Mongoid.load!('./mongoid.yml')

require_relative 'models/User'
require_relative 'models/Comments'

enable :sessions

helpers do
  include Rack::Utils
  alias_method :esc, :escape_html
end

before do
  if session[:userid]
    @user = User.where(_id: session[:userid]).first
  end
end

def changeBr(str)
  str.gsub(/(\r\n|\r|\n)/, "<br />")
end

def gravatar_img(user)
  gravatar_id  = Digest::MD5.hexdigest(user.address.downcase)
  gravatar_url ="http://www.gravatar.com/avatar/#{gravatar_id}"
end

get '/user_regist' do
  erb :user_regist
end

post '/user_regist' do
  if params[:password] != params[:check_pass]
    redirect '/user_regist'
  end

  user = User.new(
         userid: params[:userid],
         username: params[:username],
         address: params[:address]
         )
  user.encrypt_password(params[:password])

  if user.save!
    redirect '/login'
  else
    redirect '/user_regist'
  end
end

get '/login' do
    if session[:userid]
      redirect '/'
    end

  erb :login
end

post '/session' do
  if session[:userid]
    redirect '/'
  end

  user = User.authenticat(params[:userid], params[:password])
  if user
    session[:userid] = user._id
    redirect '/'
  else
    redirect '/login'
  end
end

delete '/session' do
  session[:userid] = nil
  redirect '/login'
end

get '/' do
  if @user
    erb :index
  else
    redirect '/login'
  end
end

post '/' do
  contents = Comments.new(
             userid:   params[:userid],
             username: params[:username],
             comments: params[:comment]
             )
  contents.save
  redirect '/'
end

delete '/delete' do
  Comments.delete(params[:id])
  redirect '/'
end

post '/search' do
  @search = Comments.search(params[:search_word])
  erb :result
end

get '/plofile' do
  if @user
    erb :plofile
  else
    redirect '/login'
  end
end

post '/plofile' do
  @user.update_attributes!(
               username: params[:username],
               plofile: params[:plofile]
               )
  redirect '/plofile'
end

get '/:others_userid' do
  @userinfo = User.where(userid: params[:others_userid]).first
  @usertweet = Comments.where(userid: params[:others_userid])
  if @userinfo && @usertweet
    erb :userinfo
  end
end