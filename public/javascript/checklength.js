(function () {
  var MAX_TWEET = {'VALUE': 140};
  Object.freeze(MAX_TWEET);

  var button = document.getElementById('send-tweet');
  var lengthNum = document.getElementById('tweet-length');

  document.getElementById('tweet').addEventListener('keyup', function(){
    lengthNum.textContent = this.value.length;
    if (this.value.length > MAX_TWEET.VALUE) {
      lengthNum.style.color = "red";
      button.disabled = 'true';
    } else {
      lengthNum.style.color = "black";
      button.disabled = '';
    }
  });
})();