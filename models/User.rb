require 'mongoid'
require 'bcrypt'
Mongoid.load!('./mongoid.yml')

class User
  include Mongoid::Document
  field :userid,   type: String
  field :username, type: String
  field :plofile,  type: String
  field :address,  type: String
  field :password_hash, type: String
  field :password_salt, type: String

  attr_readonly :password_hash, :password_salt

  validates_presence_of :userid
  validates_uniqueness_of :userid
  validates_presence_of :username
  validates_presence_of :address
  validates_confirmation_of :password_hash
  validates_presence_of :password_hash
  validates_presence_of :password_salt

  def self.authenticat(userid, password)
    user = self.where(userid: userid).first
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_hash)
      user
    else
      nil
    end
  end

  def encrypt_password(password)
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
end