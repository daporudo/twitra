require 'mongoid'
Mongoid.load!('./mongoid.yml')

class Comments
  include Mongoid::Document
  field :userid,   type: String
  field :username, type: String
  field :comments, type: String
  field :postdate, type: DateTime, default: lambda { Time.now }

  def self.search(search_word)
    search = self.where(comments:"#{search_word}")
    return search
  end

  def self.delete(id)
    comments = self.where(_id: "#{id}").first
    comments.destroy
  end
end
